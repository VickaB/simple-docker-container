# Simlpe Docker Container
## A simple Docker container ready for Symfony framework development


### Images

* php:8.1.0RC3-fpm-alpine3.14
* nginx:alpine
* mariaDB:latest
* phpMyAdmin:latest

### Php
* Composer
* Symfony Cli
* xdebug
* opcache

### Installation and Configuration

1. Download zip or clone repository then, depending if:
   - You're creating a new Symfony Project :
     1. Go to your-project/app_env and run `docker-compose up --build` to get your containers ready
     2. Run `symfony new .` . Your project will be created in your-project/app_src directory
   - You're installing an existing Symfony Project (This part is yet not tested but it is not a big deal, I'll complete it soon):
     1. Put your project files in your-project/app_src directory .
     2. Go to your-project/app_env and set the configuration values in container .env file.
     3. Uncomment line 19 in your-project/app_env/php-fpm/Dockerfile
     4. Run `docker-compose up --build` Your project will be installed in your-project/app_src directory.
2.  Go to [localhost:8000](http://localhost:8000) for your project home and [localhost:8081](http://localhost:8081)  for phpMyAdmin access.

Actually there's more to do with the configuration and usage.

### Thanks

I did this by following great tutorials by:
[Damien](https://medium.com/@dam1enWeb/symfony-5-2-et-php-8-sous-docker-en-5-minutes-942ca6e94ce0)
[Martin](https://dev.to/martinpham/symfony-5-development-with-docker-4hj8)
[inanzz.com](http://www.inanzzz.com/index.php/post/isb1/alpine-docker-setup-for-symfony-applications)
[Pierre](https://blog.pierrebelin.fr/create-docker-symfony-mariadb-nginx/)

